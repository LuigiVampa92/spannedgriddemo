package com.luigivampa92.spannedgriddemo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class DemoRecyclerViewAdapter (
    private val onItemClicked: ((DemoItem) -> Unit)? = null
) : RecyclerView.Adapter<DemoViewHolder>() {

    private var items: ArrayList<DemoItem> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DemoViewHolder(LayoutInflater.from(parent.context), parent, onItemClicked)

    override fun onBindViewHolder(holder: DemoViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    fun setItems(items: List<DemoItem>) {
        this.items = ArrayList(items)
        notifyDataSetChanged()
    }

    fun addItems(items: List<DemoItem>) {
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun provideSpanInfoForItemAtPosition(position: Int): SpannedGridLayoutManager.SpanInfo =
        if (position <= items.size) {
            val item = items[position]
            val spans: Pair<Int, Int> = when (item.format) {
                DemoItemFormat.SQUARE_BIG -> Pair(2, 2)
                DemoItemFormat.RECTANGLE_HORIZONTAL -> Pair(2, 1)
                DemoItemFormat.RECTANGLE_VERTICAL -> Pair(1, 2)
                else -> Pair(1, 1)
            }
            SpannedGridLayoutManager.SpanInfo(spans.first, spans.second)
        } else {
            SpannedGridLayoutManager.SpanInfo(1, 1)
        }
}