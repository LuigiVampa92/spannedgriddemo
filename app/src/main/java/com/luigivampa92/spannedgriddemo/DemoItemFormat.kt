package com.luigivampa92.spannedgriddemo

enum class DemoItemFormat {
    SQUARE_SMALL,
    SQUARE_BIG,
    RECTANGLE_HORIZONTAL,
    RECTANGLE_VERTICAL
}