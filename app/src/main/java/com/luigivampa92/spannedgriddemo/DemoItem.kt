package com.luigivampa92.spannedgriddemo

data class DemoItem (
    val format: DemoItemFormat,
    val imagrUrl: String,
    val stringPrimary: String,
    val stringSecondary: String
)