package com.luigivampa92.spannedgriddemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var layoutManager: SpannedGridLayoutManager
    private lateinit var adapter: DemoRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerView = findViewById(R.id.recycler_view)
        adapter = DemoRecyclerViewAdapter(this::itemClicked)
        layoutManager = SpannedGridLayoutManager(adapter::provideSpanInfoForItemAtPosition, 2, 0.75f)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
        showTestData()
    }

    private fun showTestData() {
        val items = listOf(
            DemoItem(DemoItemFormat.SQUARE_SMALL, "stub", "One", "odin"),
            DemoItem(DemoItemFormat.SQUARE_SMALL, "stub", "Two", "dva"),
            DemoItem(DemoItemFormat.RECTANGLE_VERTICAL, "stub", "Three", "tri"),
            DemoItem(DemoItemFormat.SQUARE_SMALL, "stub", "Four", "chetire"),
            DemoItem(DemoItemFormat.SQUARE_SMALL, "stub", "Five", "pyat"),
            DemoItem(DemoItemFormat.RECTANGLE_HORIZONTAL, "stub", "Six", "shest"),
            DemoItem(DemoItemFormat.RECTANGLE_HORIZONTAL, "stub", "Seven", "sem"),
            DemoItem(DemoItemFormat.SQUARE_SMALL, "stub", "Eight", "vosem"),
            DemoItem(DemoItemFormat.RECTANGLE_VERTICAL, "stub", "Nine", "devyat"),
            DemoItem(DemoItemFormat.SQUARE_SMALL, "stub", "Ten", "desyat"),
            DemoItem(DemoItemFormat.SQUARE_BIG, "stub", "Eleven", "odinnadtsat")
        )

        adapter.setItems(items)
    }

    private fun itemClicked(item: DemoItem) {
        toast(item.stringPrimary)
    }

    private fun toast(message: String?) {
        message?.let {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        }
    }
}
