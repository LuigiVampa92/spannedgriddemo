package com.luigivampa92.spannedgriddemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class DemoViewHolder(
    inflater: LayoutInflater,
    container: ViewGroup,
    private val onItemClicked: ((DemoItem) -> Unit)? = null
) : RecyclerView.ViewHolder(inflater.inflate(R.layout.item_demo, container, false)) {

    private val imageViewMain: View = itemView.findViewById(R.id.img_main)
    private val textViewPrimary: TextView = itemView.findViewById(R.id.text_primary)
    private val textViewSecondary: TextView = itemView.findViewById(R.id.text_secondary)

    fun bind(item: DemoItem) {
        textViewPrimary.text = item.stringPrimary
        textViewSecondary.text = item.stringSecondary
        imageViewMain.setOnClickListener {
            onItemClicked?.invoke(item)
        }
    }
}