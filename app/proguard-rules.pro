# ----- AndroidX -----
-dontwarn androidx.**
-dontwarn com.google.android.material.**
-keep class androidx.** { *; }
-keep interface androidx.** { *; }
-keep class com.google.android.material.** { *; }

# ----- AppCompat ----- (на всякий пожарный)
-dontwarn android.support.v4.**
-dontwarn android.support.v7.**
-keep class android.support.v4.** { *; }
-keep class android.support.v7.** { *; }

